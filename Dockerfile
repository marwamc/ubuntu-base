ARG DISTRO_NAME=ubuntu
ARG DISTRO_VERSION=noble
FROM ${DISTRO_NAME}:${DISTRO_VERSION}

## common environment variables
ENV DEBIAN_FRONTEND=noninteractive \
    PUSER=moran \
    PUID=1003 \
    PGID=1003 \
    SHELL=/bin/bash \
    S6_ARCH="x86_64" \
    S6_VERSION="v3.1.5.0"

ENV HOME=/home/${PUSER} \
    HEARTBEAT_FREQ="600s"

USER root
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

## Copy a script that we will use to correct permissions after running certain commands
COPY fix-permissions /usr/local/bin/fix-permissions
RUN set -eux && chmod a+rx /usr/local/bin/fix-permissions

## install - useful linux packages
RUN set -eux \
    && apt-get -yq update \
    && apt-get -yq install --no-install-recommends \
        apt-transport-https \
        bash \
        ca-certificates \
        curl \
        locales \
        sudo \
        tar \
        tzdata \
        xz-utils \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

## create user and personalize
RUN set -eux \
    # Enable prompt color in the skeleton .bashrc before creating the default user
    && sed -i 's/^#force_color_prompt=yes/force_color_prompt=yes/' /etc/skel/.bashrc \
    # Add call to conda init script see https://stackoverflow.com/a/58081608/4413446
    && echo 'eval "$(command conda shell.bash hook 2> /dev/null)"' >> /etc/skel/.bashrc \
    # create user and ensure access to required directories
    && echo "auth requisite pam_deny.so" >> /etc/pam.d/su \
    && sed -i.bak -e 's/^%admin/#%admin/' /etc/sudoers \
    && sed -i.bak -e 's/^%sudo/#%sudo/' /etc/sudoers \
    && useradd \
        --create-home \
        --shell /bin/bash \
        --user-group \
        --uid "${PUID}" \
        "${PUSER}" \
    && chmod g+w /etc/passwd \
    && fix-permissions "${HOME}" \
    && fix-permissions /usr/local/bin \
    && fix-permissions /tmp \
    && apt-get clean

## install - s6 overlay
RUN set -eux \
    && curl -fsSL "https://github.com/just-containers/s6-overlay/releases/download/${S6_VERSION}/s6-overlay-noarch.tar.xz" -o /tmp/s6-overlay-noarch.tar.xz \
    && curl -fsSL "https://github.com/just-containers/s6-overlay/releases/download/${S6_VERSION}/s6-overlay-noarch.tar.xz.sha256" -o /tmp/s6-overlay-noarch.tar.xz.sha256 \
    && echo "$(cat /tmp/s6-overlay-noarch.tar.xz.sha256 | awk '{ print $1; }')  /tmp/s6-overlay-noarch.tar.xz" | sha256sum -c - \
    && curl -fsSL "https://github.com/just-containers/s6-overlay/releases/download/${S6_VERSION}/s6-overlay-${S6_ARCH}.tar.xz" -o /tmp/s6-overlay-${S6_ARCH}.tar.xz \
    && curl -fsSL "https://github.com/just-containers/s6-overlay/releases/download/${S6_VERSION}/s6-overlay-${S6_ARCH}.tar.xz.sha256" -o /tmp/s6-overlay-${S6_ARCH}.tar.xz.sha256 \
    && echo "$(cat /tmp/s6-overlay-${S6_ARCH}.tar.xz.sha256 | awk '{ print $1; }')  /tmp/s6-overlay-${S6_ARCH}.tar.xz" | sha256sum -c - \
    && tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz \
    && tar -C / -Jxpf /tmp/s6-overlay-${S6_ARCH}.tar.xz \
    && rm /tmp/s6-overlay-noarch.tar.xz  \
            /tmp/s6-overlay-noarch.tar.xz.sha256 \
            /tmp/s6-overlay-${S6_ARCH}.tar.xz \
            /tmp/s6-overlay-${S6_ARCH}.tar.xz.sha256

## set locale configs
RUN set -eux \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8

## s6 - copy scripts
COPY --chown=${PUSER}:users --chmod=755 s6/ /etc

USER ${PUID}
WORKDIR $HOME

ENTRYPOINT ["/init"]
