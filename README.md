# ubuntu-base

an ubuntu base, with a defined runtime user and home directory ownership for that user. 
inspired by the [kubeflow notebook server base image][kfpb].

# included
The following libraries/packages are included in this ubuntu-base:

1. [s6-overlay version=v3.1.5.0](https://github.com/just-containers/s6-overlay)


[kfpb]: https://github.com/kubeflow/kubeflow/blob/master/components/example-notebook-servers/base/Dockerfile
